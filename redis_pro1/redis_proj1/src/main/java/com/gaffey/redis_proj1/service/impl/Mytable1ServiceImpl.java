package com.gaffey.redis_proj1.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gaffey.redis_proj1.domain.Mytable1;
import com.gaffey.redis_proj1.service.Mytable1Service;
import com.gaffey.redis_proj1.mapper.Mytable1Mapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author GaffEy
* @description 针对表【mytable1】的数据库操作Service实现
* @createDate 2024-02-29 21:01:18
*/
@Service
public class Mytable1ServiceImpl extends ServiceImpl<Mytable1Mapper, Mytable1>
    implements Mytable1Service {

	@Resource
	Mytable1Mapper mytable1Mapper;
	@Override
	public String insertUser(String userName, String userPassword) {
		Mytable1 user = new Mytable1();
		user.setName(userName);
		user.setPwd(userPassword);
		int insert = mytable1Mapper.insert(user);
		return userName + "注册成功";

	}

	@Override
	public String userLogin(String userName, String userPassword) {
		QueryWrapper<Mytable1> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("name",userName);
		queryWrapper.eq("pwd",userPassword);
		System.out.println(queryWrapper);
		Mytable1 user = mytable1Mapper.selectOne(queryWrapper);
		if(user == null){
			return null;
		}
		return user.toString();
	}
}




