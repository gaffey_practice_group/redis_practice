package com.gaffey.redis_proj1;


import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class CookieUtils {
	public void addCookie(HttpServletRequest request, HttpServletResponse response, String name, String value, int maxAge){
		Cookie[] cookies = request.getCookies();
		Cookie cookie = new Cookie(name,value);
		cookie.setMaxAge(maxAge);
		cookie.setPath("/");
		response.addCookie(cookie);
	}

	public Cookie getCookie(HttpServletRequest request,String name){
		String cookieValue = null;
		Cookie[] cookies = request.getCookies();
		if(cookies != null && cookies.length > 0){
			for (Cookie cookie : cookies){
				if(cookie.getName().equals(name)){
					return cookie;
				}
			}
		}
		return null;
	}

	public String getCookieValue(HttpServletRequest request,String cookieName){
		String cookieValue = null;
		Cookie[] cookies = request.getCookies();
		if(cookies != null && cookies.length > 0){
			for (Cookie cookie : cookies){
				if(cookie.getName().equals(cookieName)){
					cookieValue = cookie.getValue();
				}
			}
		}
		return cookieValue;
	}

	public boolean removeCookie(HttpServletRequest request,HttpServletResponse response,String name){
		Cookie cookie = getCookie(request,name);
		if(cookie != null){
			System.out.println("将cookieMaxAge设为0: "+cookie.getName());
//			cookie = new Cookie(cookie.getName(),null);
			System.out.println(cookie.getMaxAge());
			cookie.setMaxAge(0);
			cookie.setPath("/");
			/**
			 * 设置 cookie的MaxAge为0时，必须也要设置 setPath，不然不会失效
			 */
			System.out.println(cookie.getMaxAge());
			response.addCookie(cookie);

			return true;
		}
		return false;
	}
}
