package com.gaffey.redis_proj1.service;

import com.gaffey.redis_proj1.domain.Mytable1;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
* @author GaffEy
* @description 针对表【mytable1】的数据库操作Service
* @createDate 2024-02-29 21:01:18
*/
@Service
public interface Mytable1Service extends IService<Mytable1> {
	public String insertUser(String userName,String userPassword);

	public String userLogin(String userName,String userPassword);
}
