package com.gaffey.redis_proj1;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.gaffey.redis_proj1.mapper")
@SpringBootApplication
public class RedisProj1Application {

	public static void main(String[] args) {
		SpringApplication.run(RedisProj1Application.class, args);
	}

}
