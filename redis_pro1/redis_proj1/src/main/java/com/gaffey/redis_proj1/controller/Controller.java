package com.gaffey.redis_proj1.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.gaffey.redis_proj1.CookieUtils;
import com.gaffey.redis_proj1.User;
import com.gaffey.redis_proj1.service.Mytable1Service;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


@RestController
@RequestMapping("/redis")
@Data
public class Controller {

	@Autowired
	RedisTemplate redisTemplate;

	@Autowired
	CookieUtils cookieUtils;

	@Resource
	Mytable1Service mytable1Service;


	@PostMapping("/register")
	public String Register(@RequestBody User user){
		String checkUser = mytable1Service.userLogin(user.getUserName(), user.getUserPassword());
		if(checkUser!=null){
			return "用户已存在";
		}else {
			String s = mytable1Service.insertUser(user.getUserName(), user.getUserPassword());
			return s+"注册成功";
		}
	}

	@PostMapping ("/login")
	public String Login(HttpServletRequest request, HttpServletResponse response,@RequestBody User user) throws JsonProcessingException {
		System.out.println("User Information"+user);
		ValueOperations ops = redisTemplate.opsForValue();
		String str = cookieUtils.getCookieValue(request, user.getUserName());
		System.out.println("token: " + str);
		if(str != null && !str.equals("")){
			if(redisTemplate.hasKey(str)){
				//已登录
				return "重复登录";
			}else {
				boolean removeCookie = cookieUtils.removeCookie(request, response, user.getUserName());
				if(removeCookie){
//					System.out.println("移除Cookie");
				}
				return "信息异常，不允许登录";
			}
		}else {
			String userLogin = mytable1Service.userLogin(user.getUserName(), user.getUserPassword());
			if(userLogin!=null){
				String uuid = UUID.randomUUID().toString();
				cookieUtils.addCookie(request,response,user.getUserName(),uuid,60 * 30);
				ObjectMapper om = new ObjectMapper();
				ops.set(uuid,om.writeValueAsString(user.getUserName()));
				redisTemplate.expire(uuid,30, TimeUnit.MINUTES);//过期时间
				return "登录成功";
			}else {
				return "用户未注册";
			}
		}
	}

	@PostMapping("/logout")
	public String Logout(HttpServletRequest request,HttpServletResponse response,@RequestBody User user) throws JsonProcessingException{
		System.out.println("user: "+user);
//		System.out.println(user);
		String str = cookieUtils.getCookieValue(request, user.getUserName());
		System.out.println(str);
		if(redisTemplate.delete(str)){
			cookieUtils.removeCookie(request,response, user.getUserName());
			return "成功退出登录";
		}else {
			return "系统异常";
		}
	}

}
