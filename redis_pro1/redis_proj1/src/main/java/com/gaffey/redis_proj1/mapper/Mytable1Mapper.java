package com.gaffey.redis_proj1.mapper;

import com.gaffey.redis_proj1.domain.Mytable1;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author GaffEy
* @description 针对表【mytable1】的数据库操作Mapper
* @createDate 2024-02-29 21:01:18
* @Entity generator.domain.Mytable1
*/
public interface Mytable1Mapper extends BaseMapper<Mytable1> {

}




