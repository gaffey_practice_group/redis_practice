package com.gaffey.redis_proj1.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gaffey.redis_proj1.domain.Mytable1;
import com.gaffey.redis_proj1.mapper.Mytable1Mapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class Mytable1ServiceTest {

	@Resource
	Mytable1Mapper mytable1Mapper;
	@Test
	public void test(){
		QueryWrapper<Mytable1> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("name","gaf");

		List<Mytable1> mytable1s = mytable1Mapper.selectList(queryWrapper);
		for (Mytable1 x: mytable1s
			 ) {
			System.out.println(x);
		}
		System.out.println("end");
	}
}