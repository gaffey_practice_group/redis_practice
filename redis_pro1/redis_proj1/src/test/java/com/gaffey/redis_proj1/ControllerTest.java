package com.gaffey.redis_proj1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ControllerTest {
	@Autowired
	RedisTemplate redisTemplate;

	@Test
	void contextLoads() throws JsonProcessingException {
		ObjectMapper om = new ObjectMapper();
		ValueOperations ops = redisTemplate.opsForValue();
		ops.set("uuid",om.writeValueAsString("user"));
		redisTemplate.expire("uuid",30, TimeUnit.MINUTES);//30分钟过期
	}

}